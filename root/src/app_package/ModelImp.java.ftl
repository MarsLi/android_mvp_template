package ${packageName};

public class ${className}ModelImp implements ${className}Model {

    private ${serviceName} service;

    private ${serviceName} getService() {
        if (service == null) {
            service = ServiceGenerator.createService(${serviceName}.class);
        }
        return service;
    }

}
