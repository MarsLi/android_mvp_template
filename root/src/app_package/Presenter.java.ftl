package ${packageName};

public class ${className}PresenterImp implements ${className}Contract.Presenter {

private ${className}Contract.View mView;

public ${className}PresenterImp(${className}Contract.View mView) {
this.mView = mView;
}
}