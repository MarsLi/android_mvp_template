package ${packageName};

import butterknife.ButterKnife;

import android.os.Bundle;
import android.support.annotation.Nullable;

public class ${className}Activity extends BaseActivity implements ${className}Contract.View {

private ${className}Contract.Presenter presenter;

@Override
protected void onCreate(@Nullable Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.${layoutName});
ButterKnife.bind(this);
presenter = new ${className}PresenterImp(this);

initViews();
}

private void initViews() {

}
}
