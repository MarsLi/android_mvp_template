<?xml version="1.0"?>
<recipe>

    <merge from="AndroidManifest.xml.ftl"
           to="${escapeXmlAttribute(manifestOut)}/AndroidManifest.xml" />

    <instantiate from="src/app_package/Contract.java.ftl"
                 to="${escapeXmlAttribute(srcOut)}/${className}Contract.java" />

    <instantiate from="src/app_package/Activity.java.ftl"
                 to="${escapeXmlAttribute(srcOut)}/${className}Activity.java" />

    <instantiate from="src/app_package/Model.java.ftl"
                 to="${escapeXmlAttribute(srcOut)}/${className}Model.java" />

    <instantiate from="src/app_package/ModelImp.java.ftl"
                 to="${escapeXmlAttribute(srcOut)}/${className}ModelImp.java" />

    <instantiate from="src/app_package/Presenter.java.ftl"
                 to="${escapeXmlAttribute(srcOut)}/${className}PresenterImp.java" />

    <instantiate from="res/layout/activity_layout.xml.ftl"
                 to="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml" />

    <open file="${srcOut}/${className}Presenter.java"/>
</recipe>